﻿//PointGraphic
//main.cpp

#define VER ((string)"v1.3.4")

#include <iostream>
#include <fstream>
#include <string>
#include <glut.h>
#include <getopt.h>
#include <vector>
#include <math.h>

using namespace std;

class point2d {
private:
	double x, y;

public:
	void setX(double v) { x = v; }
	void setY(double v) { y = v; }
	double getX() { return x; }
	double getY() { return y; }
};

class pointKit {
private:
	vector<point2d> data;
	double color[3];
	double minX, maxX, minY, maxY;
	string colorName, fileName, legendName;

public:
	void setData(char* path) {

		fileName = path;

		int n = 0;
		double d;
		point2d p;

		cout << "\tfile: " << path;

		ifstream file;
		file.open(path);

		if (!file) {
			cout << "\tfile was not open!" << endl;
			exit(-1);
		}
		else
			cout << "\tis opened" << endl;

		while (!file.eof()) {
			file >> d;
			n++;
		}

		if (n % 2)
			n--;

		double** arr = new double*[n / 2];
		for (int i = 0; i < n / 2; i++)
			arr[i] = new double[2];

		file.clear();
		file.seekg(0);

		vector<point2d> ret;

		for (int i = 0; i < n / 2; i++) {
			file >> d;
			p.setX(d);
			file >> d;
			p.setY(d);
			ret.push_back(p);
		}
		file.close();

		data = ret;
	}

	void setColor(int c) {
		switch (c) {
		case 0:
			color[0] = 1.0; color[1] = 0.0; color[2] = 0.0;
			colorName = "RED";
			break;
		case 1:
			color[0] = 0.0; color[1] = 1.0; color[2] = 0.0;
			colorName = "GREEN";
			break;
		case 2:
			color[0] = 0.0; color[1] = 0.0; color[2] = 1.0;
			colorName = "BLUE";
			break;
		case 3:
			color[0] = 0.0; color[1] = 1.0; color[2] = 1.0;
			colorName = "CYAN";
			break;
		case 4:
			color[0] = 1.0; color[1] = 0.0; color[2] = 1.0;
			colorName = "MAGENTA";
			break;
		case 5:
			color[0] = 1.0; color[1] = 1.0; color[2] = 0.0;
			colorName = "YELLOW";
			break;
		case 6:
			color[0] = 0.0; color[1] = 0.0; color[2] = 0.0;
			colorName = "BLACK";
			break;
		}

		cout << "\t" << fileName << "\t" << colorName << endl;

	}

	void setLegendName(string name) { legendName = name; }

	void findMaxMin() {
		minX = data[0].getX();
		minY = data[0].getY();
		maxX = data[0].getX();
		maxY = data[0].getY();

		for (int i = 0; i < data.size(); i++) {
			if (data[i].getX() < minX)
				minX = data[i].getX();
			else if (data[i].getX() > maxX)
				maxX = data[i].getX();
			if (data[i].getY() < minY)
				minY = data[i].getY();
			else if (data[i].getY() > maxY)
				maxY = data[i].getY();
		}
	}

	void printData() {
		for (auto s : data)
			cout << s.getX() << "\t" << s.getY() << endl;

	}

	void printLegend() { cout << legendName << endl; }

	vector<point2d>& getData() { return data; }
	double* getColor() { return color; }
	string getColorName() { return colorName; }
	string getLegendName() { return legendName; }
	double getMinX() { return minX; }
	double getMaxX() { return maxX; }
	double getMinY() { return minY; }
	double getMaxY() { return maxY; }
};

struct parsedCharData {
	int k = 0;
	char** data;
};

struct standartDeltaStep {
	double delta;
	double step;
};

int f = 0;

int W = 800;
int H = 600;

int ax = 9; //[px]
int ay = 10; //[px]

double symbol_X, symbol_Y; //[v]

double dx, dy; //[v/px]

double DOPX = 0.3;
double DOPY = 0.3;

double dopx, dopy;

double _STEPX = 0, _STEPY = 0;
double START_X, END_X, START_Y, END_Y;

char SEP[] = ",";

double MIN_X, MAX_X, MIN_Y, MAX_Y;

string textX = "";
string textY = "";
string title = "";

parsedCharData fileNames, legendNames;

vector<pointKit> fileArray;

vector<point2d>& getPointsFromFile(string path) {
	int n = 0;
	double d;
	point2d p;

	cout << "\tfile: " << path;

	ifstream file;
	file.open(path);

	if (!file) {
		cout << "\tfile was not open!" << endl;
		exit(-1);
	}
	else
		cout << "\tis opened" << endl;

	while (!file.eof()) {
		file >> d;
		n++;
	}

	if (n % 2)
		n--;

	double** arr = new double* [n/2];
	for (int i = 0; i < n/2; i++)
		arr[i] = new double[2];

	file.clear();
	file.seekg(0);

	vector<point2d> ret;

	for (int i = 0; i < n/2; i++) {
		file >> d;
		p.setX(d);
		file >> d;
		p.setY(d);
		ret.push_back(p);
	}
	file.close();

	return ret;
}

vector<pointKit> getFileArray(parsedCharData fileNames) {

	vector<pointKit> ret;
	pointKit tmp;

	for (int i = 0; i < fileNames.k; i++) {
		tmp.setData(fileNames.data[i]);
		ret.push_back(tmp);
	}
	return ret;
}

void __glutInit(int* argc, char** argv) {
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(W, H);
	glutInitWindowPosition(100, 200);
	glutCreateWindow(((string)"PointGraphic " + VER + "   " + to_string(W) + "   " + to_string(H)).data());
}

void calcGparams() {
	if (DOPX > 0.5 || DOPX <= 0)
		DOPX = 0.3;
	if (DOPY > 0.5 || DOPY <= 0)
		DOPY = 0.3;
	dopx = (END_X - START_X) * DOPX; //[v]
	dopy = (END_Y - START_Y) * DOPY; //[v]
	dx = (END_X - START_X + 2*dopx) / W;
	dy = (END_Y - START_Y) / H;
	symbol_X = ax * dx;
	symbol_Y = ay * dy;
}

void Initialize() {
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	calcGparams();
	glOrtho(START_X-(legendNames.k == fileArray.size() ? 0.8*dopx : dopx), END_X+(legendNames.k == fileArray.size() ? 1.2*dopx : dopx), START_Y-dopy, END_Y+dopy, -1, 1);
}

void drawText(const char* text, int length, double x, double y, string mode) {
	//cout << "*" << endl;
	glMatrixMode(GL_PROJECTION);
	double *matrix = new double[16];
	glGetDoublev(GL_PROJECTION_MATRIX, matrix);
	glLoadIdentity();
	glOrtho(START_X - (legendNames.k == fileArray.size() ? 0.8*dopx : dopx), END_X + (legendNames.k == fileArray.size() ? 1.2*dopx : dopx), START_Y - dopy, END_Y + dopy, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	glLoadIdentity();


	if (mode == "-x") {
		glRasterPos2f(START_X + ((END_X - START_X)/2 - symbol_X*length/2), START_Y - (50 * dy));
		for (int i = 0; i < length; i++) {
			if (text[i] == '_')
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)' ');
			else
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
		}
	}
	else if (mode == "-y") {
		//int upperCase = 0; //это на будущее
		glRasterPos2f(START_X - symbol_X*length/2, END_Y + dy * 28);
		for (int i = 0; i < length; i++) {
			if (text[i] == '_')
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)' ');
			else
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
		}
	}
	else if (mode == "-l") {
		glRasterPos2f(END_X + 35 * dx, (END_Y - 44 * dy) - 40 * dy * y);
		for (int i = 0; i < length; i++) {
			if (text[i] == '_')
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)' ');
			else
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
		}
	}
	else if (mode == "-t") {
		glRasterPos2f(START_X + ((END_X - START_X) / 2 - symbol_X*length/2), END_Y + dy * 100);
		for (int i = 0; i < length; i++) {
			if (text[i] == '_')
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)' ');
			else
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
		}
	}

	else if (mode == "X") {
		int n = length - 1;
		while ((text[n] == '0') && (text[n] != '.'))
			n--;
		if (text[n] == '.')
			n--;

		if ((n == 1) && (text[0] == '-') && (text[1] == '0')) { //если 0 со знаком
			glRasterPos2f(x - symbol_X/2, START_Y - (27 * dy));
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)'0');
		}
		else {
			glRasterPos2f(x - (symbol_X*(n + 1)/2), START_Y - (27 * dy));
			for (int i = 0; i < (n + 1); i++)
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
		}
	}
	else if (mode == "Y") {
		int n = length - 1; unsigned char* newStr;
		while ((text[n] == '0') && (text[n] != '.'))
			n--;
		if (text[n] == '.')
			n--;

		if ((n == 1) && (text[0] == '-') && (text[1] == '0')) { //если 0 со знаком
			/*newStr = new unsigned char[2];
			newStr[0] = '0';
			newStr[1] = '\0';
			int l = glutBitmapLength(GLUT_BITMAP_9_BY_15, newStr);*/
			glRasterPos2f(START_X - (symbol_X + 10 * dx), y - symbol_Y/2);
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)'0');
		}

		else {
			/*newStr = new unsigned char[n + 2];

			for (int i = 0; i < (n + 1); i++)
				newStr[i] = text[i];
			newStr[n + 1] = '\0';
			int l = glutBitmapLength(GLUT_BITMAP_9_BY_15, newStr);

			for (int i = 0; i < (n + 1); i++)
				cout << newStr[i];
			cout << endl << l << endl;*/

			glRasterPos2f(START_X - ((n+1) * symbol_X + 10 * dx), y - symbol_Y / 2);
			for (int i = 0; i < (n + 1); i++)
				glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
		}
	}

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(matrix);
	glMatrixMode(GL_MODELVIEW);
}

void drawGraph(pointKit& arr) {
	double* color = arr.getColor();

	glColor3f(color[0], color[1], color[2]);
	glBegin(GL_LINES);
	for (unsigned int i = 0; i < arr.getData().size() - 1; i++) {
		glVertex2f(arr.getData()[i].getX(), arr.getData()[i].getY());
		glVertex2f(arr.getData()[i + 1].getX(), arr.getData()[i + 1].getY());
	}
	glEnd();
}

void Draw() {
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINES);

	for (double pos = START_Y; pos <= END_Y; pos += _STEPY) { //отрисовка горизонтальных линий сетки
		glVertex2f(START_X, pos);
		glVertex2f(END_X, pos);
	}

	for (double pos = START_X; pos <= END_X; pos += _STEPX) { //отрисовка вертикальных линий сетки
		glVertex2f(pos, START_Y);
		glVertex2f(pos, END_Y);
	}

	glEnd();

	for (auto s : fileArray)
		drawGraph(s);

	glColor3f(0.0, 0.0, 1.0);
	string dToStr;
	int i = 0;
	for (double pos = START_Y; pos <= END_Y; pos += _STEPY, i++) { //вывод значений вертикальной оси
		dToStr = to_string(pos);
		drawText(dToStr.data(), dToStr.size(), 0, pos, "Y");
	}

	i = 0;
	for (double pos = START_X; pos <= END_X; pos += _STEPX, i++) { //вывод значений горизонтальной оси
		dToStr = to_string(pos);
		drawText(dToStr.data(), dToStr.size(), pos, 0, "X");
	}
	glColor3f(0.0, 0.0, 0.0);
	drawText(textX.data(), textX.size(), 0, 0, "-x");	//подпись горизонтальной оси
	drawText(textY.data(), textY.size(), 0, 0, "-y");//подпись вертикальной оси

	if (legendNames.k == fileArray.size()) //вывод легенды
		for (unsigned int i = 0; i < fileArray.size(); i++) {
			glColor3f(fileArray[i].getColor()[0], fileArray[i].getColor()[1], fileArray[i].getColor()[2]);
			glBegin(GL_LINES);
			glVertex2f(END_X + 10 * dx, END_Y - 40 * dy * (i + 1));
			glVertex2f(END_X + 30 * dx, END_Y - 40 * dy * (i + 1));
			glEnd();
			glColor3f(0.0, 0.0, 0.0);
			drawText(fileArray[i].getLegendName().data(), fileArray[i].getLegendName().size(), 0, i, "-l");
		}

	if (title != "") //вывод заголовка
		drawText(title.data(), title.size(), 0, 0, "-t");
	glutSwapBuffers();
	//glFlush();
}

void Reshape(int w, int h) {
	
	if (w < 500) {
		w = 500;
		glutReshapeWindow(w, h);
	}
	if (h < 500) {
		h = 500;
		glutReshapeWindow(w, h);
	}

	W = w;
	H = h;

	calcGparams();

	glutSetWindowTitle(((string)"PointGraphic " + VER + "   "
		+ to_string(w) + "   " + to_string(h)).data());


	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(START_X - (legendNames.k == fileArray.size() ? 0.8*dopx : dopx), END_X + (legendNames.k == fileArray.size() ? 1.2*dopx : dopx), START_Y - dopy, END_Y + dopy, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

parsedCharData PARSE(char* str, char* sep) {

	int k = 0,  _k = 0;
	char** data;
	char* tmp;

	for (unsigned int i = 0; str[i] != '\0'; i++)
		if (str[i] == sep[0])
			k++;

	data = new char*[k + 1];

	tmp = strtok(str, sep);
	int i = 0;
	while (tmp != NULL) {
		data[i] = tmp;
		tmp = strtok(NULL, sep);
		i++;
	}

	parsedCharData ret;
	ret.data = data;
	ret.k = i;

	return ret;
} 

int powFinder(double v) {
	int ret = 0;
	double cur = v < 0 ? -v : v;

	if (cur == 0)
		return 0;
		

	else if (cur >= 10 || cur < 1) {
		if (cur >= 10) {
			while (cur >= 10 || cur < 1) {
				cur /= 10;
				ret++;
			}
		}
		else {
			while (cur >= 10 || cur < 1) {
				cur *= 10;
				ret--;
			}
		}
	}
	return ret;
}

string ftos(double v) {
	string ret;

	int n = powFinder(v);
	int i = 0;
	double v_ = v / pow(10, n);

	if (n < 0) {
		ret.push_back('0');
		ret.push_back('.');
	}

	while ((v_ - trunc(v_)) != 0) {
		cout << v_ << endl;
		if (i == n) {
			ret.push_back('.');
		}
		ret.push_back((char)trunc(v_));
		v_ -= trunc(v_);
		v_ *= 10;
		i++;
	}

	return ret;
}

//ненужная функция
double myCeil_valuableDigits(double v) {
	int n = powFinder(v);
	int k = n > 0 ? 2 : 3;
	double* r = new double[k + 1];
	double ret = 0;
	double v_ = v / pow(10, n);
	for (int i = 0; i <= k; i++) {
		r[i] = trunc(v_);
		v_ = (v_ - r[i]) * 10;
	}

	if (r[k] > 0) r[k - 1]++;
	for (int i = 0; i < k; i++)
		ret += r[i] / pow(10, i);

	ret *= pow(10, n);

	if ((pow(10, n + 1) - ret) < pow(10, n))
		ret = pow(10, n + 1);

	return ret;
}

//ненужная функция
double myFloor_valuableDigits(double v, int k) {
	int n = powFinder(v);
	double* r = new double[k];
	double ret = 0;
	double v_ = v / pow(10, n);
	for (int i = 0; i < k; i++) {
		r[i] = trunc(v_);
		v_ = (v_ - r[i]) * 10;
	}
	for (int i = 0; i < k; i++)
		ret += r[i] / pow(10, i);
	ret *= pow(10, n);
	return ret;
}

standartDeltaStep getStandartDeltaStep(double v) {
	double standartDeltas[] = {1, 1.2, 1.4, 1.5, 1.6, 1.8, 2.0, 2.5, 3.0, 3.5, 4, 5, 6, 7, 8, 10}; //16 int kStandartDeltas = 16;
	double step;
	int n = powFinder(v);

	double v_ = v / pow(10, n);
	int nummberOFDelta = 1;

	while ( (!(v_ >= standartDeltas[nummberOFDelta - 1] && v_ <= standartDeltas[nummberOFDelta])) && (nummberOFDelta < 16))
		nummberOFDelta++;
	
	double DELTA = standartDeltas[nummberOFDelta] * pow(10, n);

	switch (nummberOFDelta) {
	case 0:
		step = 0.1;
		break;
	case 1:
		step = 0.2;
		break;
	case 2:
		step = 0.2;
		break;
	case 3:
		step = 0.25;
		break;
	case 4:
		step = 0.2;
		break;
	case 5:
		step = 0.2;
		break;
	case 6:
		step = 0.25;
		break;
	case 7:
		step = 0.25;
		break;
	case 8:
		step = 0.5;
		break;
	case 9:
		step = 0.5;
		break;
	case 10:
		step = 0.5;
		break;
	case 11:
		step = 0.5;
		break;
	case 12:
		step = 1;
		break;
	case 13:
		step = 1;
		break;
	case 14:
		step = 1;
		break;
	case 15:
		step = 1;
		break;
	}

	standartDeltaStep ret;
	ret.delta = DELTA;
	ret.step = step * pow(10, n);

	//cout << endl << "standart delta: " << ret.delta << endl << endl;

	return ret;
}

void paramCalc() {
	double deltaPositive_X, deltaNegative_X, deltaPositive_Y, deltaNegative_Y;
	standartDeltaStep infX, infY;

	//-----------------------------------------------------------------------------
	deltaPositive_X = MAX_X;
	deltaNegative_X = 0 - MIN_X;
	if (deltaNegative_X > 0 && deltaPositive_X > 0) { ///////диапазон лежит и в положительной и в отрицательной частях
		if (deltaPositive_X >= deltaNegative_X) {
			infX = getStandartDeltaStep(deltaPositive_X);
			END_X = infX.delta;
			START_X = (ceil(deltaNegative_X/infX.step)) * (-infX.step);
		}
		else {
			infX = getStandartDeltaStep(deltaNegative_X);
			START_X = -infX.delta;
			END_X = (ceil(deltaPositive_X/infX.step)) * infX.step;
		}
	}
	else if (deltaNegative_X <= 0) { ///////////////////////////////диапазон лежит только в положительной части
		infX = getStandartDeltaStep(MAX_X - MIN_X);
		START_X = floor(-deltaNegative_X / infX.step) * infX.step;
		END_X = ceil(deltaPositive_X / infX.step) * infX.step;
	}
	else { /////////////////////////////////////////////////////////диапазон лежит только в отрицательной части
		infX = getStandartDeltaStep(MAX_X - MIN_X);
		START_X = floor(-deltaNegative_X / infX.step) * infX.step;
		END_X = ceil(deltaPositive_X / infX.step) * infX.step;
	}

	if (((END_X - START_X) / infX.step) > 10) {
		infX = getStandartDeltaStep(END_X - START_X);
		END_X = START_X + infX.delta;
	}
	if (END_X - MAX_X >= infX.step)
		END_X -= infX.step;
	if (MIN_X - START_X >= infX.step)
		START_X += infX.step;
	//------------------------------------------------------------------------------
	deltaPositive_Y = MAX_Y;
	deltaNegative_Y = 0 - MIN_Y;
	if (deltaNegative_Y > 0 && deltaPositive_Y > 0) { ///////диапазон лежит и в положительной и в отрицательной частях
		if (deltaPositive_Y > deltaNegative_Y) {
			infY = getStandartDeltaStep(deltaPositive_Y);
			END_Y = infY.delta;
			START_Y = (ceil(deltaNegative_Y / infY.step)) * (-infY.step);
		}
		else {
			infY = getStandartDeltaStep(deltaNegative_Y);
			START_Y = -infY.delta;
			END_Y = (ceil(deltaPositive_Y / infY.step)) * infY.step;
		}
	}
	else if (deltaNegative_Y <= 0) { ///////////////////////////////диапазон лежит только в положительной части
		infY = getStandartDeltaStep(MAX_Y - MIN_Y);
		START_Y = floor(-deltaNegative_Y / infY.step) * infY.step;
		END_Y = ceil(deltaPositive_Y / infY.step) * infY.step;
	}
	else { /////////////////////////////////////////////////////////диапазон лежит только в отрицательной части
		infY = getStandartDeltaStep(MAX_Y - MIN_Y);
		START_Y = floor(-deltaNegative_Y / infY.step) * infY.step;
		END_Y = ceil(deltaPositive_Y / infY.step) * infY.step;
	}
	if (((END_Y - START_Y) / infY.step) > 10) {
		infY = getStandartDeltaStep(END_Y - START_Y);
		END_Y = START_Y + infY.delta;
	}
	if (END_Y - MAX_Y >= infY.step)
		END_Y -= infY.step;
	if (MIN_Y - START_Y >= infY.step)
		START_Y += infY.step;
	//------------------------------------------------------------------------------

	_STEPX = infX.step;
	_STEPY = infY.step;

	cout << "MIN_X: " << MIN_X << "\t\tMAX_X: " << MAX_X << endl;
	cout << "MIN_Y: " << MIN_Y << "\t\tMAX_Y: " << MAX_Y << endl;
}

void setColors(vector<pointKit>& arr) {
	int k = 0;
	for (int i = 0; i < arr.size(); i++) {
		if (k > 6)
			k = 0;
		arr[i].setColor(k);
		k++;
	}
}

void keyParse(int argc, char** argv) {
	const char *opts = "f:w:h:a:b:x:y:l:t:";
	
	int opt;

	while ((opt = getopt(argc, argv, opts)) != -1) {
		switch (opt) {
		case 'f':
			f = 1;
			fileNames = PARSE(optarg, SEP);
			break;
		case 'w':
			W = atoi(optarg);
			break;
		case 'h':
			H = atoi(optarg);
			break;
		case 'a':
			DOPX = atof(optarg);
			break;
		case 'b':
			DOPY = atof(optarg);
			break;
		case 'x':
			textX = optarg;
			break;
		case 'y':
			textY = optarg;
			break;
		case 'l':
			legendNames = PARSE(optarg, SEP);
			break;
		case 't':
			title = optarg;
			break;
		}
	}

}

int main(int argc, char** argv) {

	cout << "PointGraphic \tversion: " << VER.data() << endl << endl;

	keyParse(argc, argv);
	
	if (!f) {
		cout << "NO FILES WAS CHOSEN" << endl;
		exit(-1);
	}

	cout << "YOUR FILES:" << endl;
	for (int i = 0; i < fileNames.k; i++)
		cout << fileNames.data[i] << endl;
	cout << endl;
	fileArray = getFileArray(fileNames); //загружаем данные
	cout << endl;
	setColors(fileArray); //устанавливаем цвета
	cout << endl;
	if(legendNames.k == fileArray.size()) //устанавливаем легенду
		for (int i = 0; i < fileArray.size(); i++)
			fileArray[i].setLegendName(legendNames.data[i]);
	for (unsigned int i = 0; i < fileArray.size(); i++) //находим максимумы и минимумы
		fileArray[i].findMaxMin();
	
	//cout << fileArray[0].getMinX() << "  " << fileArray[0].getMaxX() << "  " << fileArray[0].getMinY() << "  " << fileArray[0].getMaxY() << endl;

	MAX_X = fileArray[0].getMaxX(); //находим общие максимумы/минимумы
	MAX_Y = fileArray[0].getMaxY();
	MIN_X = fileArray[0].getMinX();
	MIN_Y = fileArray[0].getMinY();
	for (auto s : fileArray) {
		if (s.getMaxX() > MAX_X)
			MAX_X = s.getMaxX();
		if (s.getMinX() < MIN_X)
			MIN_X = s.getMinX();

		if (s.getMaxY() > MAX_Y)
			MAX_Y = s.getMaxY();
		if (s.getMinY() < MIN_Y)
			MIN_Y = s.getMinY();
	}

	paramCalc(); //вычисление оптимальных параметров графика
	
//--------------------------------------------------------------------------GLUT
	//инициализаия glut
	__glutInit(&argc, argv);
	//регистрация
	glutDisplayFunc(Draw);
	//glutIdleFunc(Draw);
	glutReshapeFunc(Reshape);
	//инициализация параметров отрисовки
	Initialize();

	glutMainLoop();
	
	return 0;
}